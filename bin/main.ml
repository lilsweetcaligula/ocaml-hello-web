open Cohttp
open Cohttp_lwt_unix


let send_with_content_type content_type ?(headers = Header.init ()) =
  Server.respond_string
    ~headers:(Header.add headers "content-type" content_type);;


let send_html_string =
  send_with_content_type "text/html";;


let send_json_string =
  send_with_content_type "application/json";;


let redirect_to url =
  Server.respond_string
    ~headers:(Header.init_with "location" url)
    ~status:(`Code 301)
    ~body:""
    ();;


let handle_howdoi _conn req _body =
  let uri        = Request.uri req in
  let needle     = Uri.get_query_param uri "q" in

  let target_url = (match needle with
                   | None   -> "https://google.com"
                   | Some q -> Uri.with_query (Uri.of_string "https://google.com/search")
                               [("q", [q])]
                               |> Uri.to_string) in

  redirect_to target_url;;


let handle_not_found _conn _req _body =
  send_json_string
    ~status:(`Code 404)
    ~body:"{\"message\": \"not found\"}"
    ();;


let handle_ping _conn _req _body =
  send_json_string
    ~status:(`Code 200)
    ~body:"{\"message\": \"pong!\"}"
    ();;


let route_request conn req body =
  let meth = req |> Request.meth
  and path = req |> Request.uri |> Uri.path in

  let handler = match meth, path with
                | `GET, "/ping"   -> handle_ping
                | `GET, "/howdoi" -> handle_howdoi
                | _               -> handle_not_found

  in handler conn req body;;


let main ?(args = [||]) () =
  let get_listening_port () =
    let default_port = 8080 in

    match args with
    | [| _; s |] -> (match Int32.of_string_opt s with
                    | None   -> default_port
                    | Some x -> Int32.to_int x)
    | _ -> default_port

  in 
  let port   = get_listening_port () in
  let server = Server.create ~mode:(`TCP (`Port port))
                             (Server.make ~callback:route_request ()) in
  begin
    print_endline ("Listening on port " ^ string_of_int port ^ "...");
    Lwt_main.run server
  end;;


  main ~args:Sys.argv ();;
